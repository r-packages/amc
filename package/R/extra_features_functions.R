## ======================================================================
## Feature engineering: Add more features beyond the DTM
## ======================================================================

#' Title
#'
#' @param sentences A vector of sentences.
#' @param type Which types of extra features should be computed? Can be any of c("LIWC", "FASTTEXT"), or "" for none.
#' @param UTID Identifier for each sentence. This is passed through to the results object.
#' @param USID Identifier variable that groups sentences to stories (if USID is not provided, all sentences are a single story)
#' @param pic_id The picture id to which a story has been written.
#' @param verbose Print process information?
#'
#' @import dplyr
#' @import quanteda
#' @import quanteda.dictionaries
#' @import caret
#' @importFrom tokenizers count_words
#' @importFrom data.table fread

computeExtraFeatures <- function(sentences, type=c("LIWC", "FASTTEXT"), UTID=NULL, USID=NULL, pic_id = NULL, verbose=TRUE) {

	if (is.null(UTID)) UTID <- 1:length(sentences)
	if (is.null(USID)) USID <- 1

	# sanity check
	if (any(table(UTID) > 1)) {
		stop("ERROR: The unique text IDs (UTID) are not unique (multiple entries).")
	}

	# aggregate sentences to stories
	df <- data.frame(sentences=sentences, USID, stringsAsFactors=FALSE)
	pictures <- df %>% group_by(USID) %>% summarise(
		story=paste(sentences, collapse=". ")
	)

	## ======================================================================
	## one-hot encode picture
	## Check whether the picture name is in the learned set.
	## Enrich provided pictures names with all learned picture names to get a full matrix
	## ======================================================================

	if (!is.null(pic_id)) pic_id <- as.character(pic_id)
	if (!is.null(pic_id) && pic_id != "") {
		learned_pics <- c("applause", "architect at desk", "beachcombers", "bicycle race", "boxer", "burglar", "canyon (TAT 11)", "couple by river", "couple sitting opposite a woman", "girlfriends in cafe with male approaching", "group (TAT 9BM)", "kennedy nixon", "lacrosse duel", "men on ship", "newpic1", "newpic10", "newpic11", "newpic12", "newpic13", "newpic14", "newpic16", "newpic17", "newpic18", "newpic20", "newpic21", "newpic22", "newpic23", "newpic24", "newpic25", "newpic26", "newpic27", "newpic28", "newpic29", "newpic3", "newpic30", "newpic31", "newpic32", "newpic33", "newpic4", "newpic5", "newpic6", "newpic7", "newpic8", "newpic9", "neymar & marcelo", "nightclub scene", "ship captain", "soccer duel", "sorrow", "trapeze artists", "violin (TAT 1)", "window (TAT 14)", "woman (TAT 9GF)", "women in laboratory")

		learned_pics_features <- c("PIC_applause", "PIC_architect.at.desk", "PIC_beachcombers", "PIC_bicycle.race", "PIC_boxer", "PIC_burglar", "PIC_canyon..TAT.11.", "PIC_couple.by.river", "PIC_couple.sitting.opposite.a.woman", "PIC_girlfriends.in.cafe.with.male.approaching", "PIC_group..TAT.9BM.", "PIC_kennedy.nixon", "PIC_lacrosse.duel", "PIC_men.on.ship", "PIC_newpic1", "PIC_newpic10", "PIC_newpic11", "PIC_newpic12", "PIC_newpic13", "PIC_newpic14", "PIC_newpic16", "PIC_newpic17", "PIC_newpic18", "PIC_newpic20", "PIC_newpic21", "PIC_newpic22", "PIC_newpic23", "PIC_newpic24", "PIC_newpic25", "PIC_newpic26", "PIC_newpic27", "PIC_newpic28", "PIC_newpic29", "PIC_newpic3", "PIC_newpic30", "PIC_newpic31", "PIC_newpic32", "PIC_newpic33", "PIC_newpic4", "PIC_newpic5", "PIC_newpic6", "PIC_newpic7", "PIC_newpic8", "PIC_newpic9", "PIC_neymar...marcelo", "PIC_nightclub.scene", "PIC_ship.captain", "PIC_soccer.duel", "PIC_sorrow", "PIC_trapeze.artists", "PIC_violin..TAT.1.", "PIC_window..TAT.14.", "PIC_woman..TAT.9GF.", "PIC_women.in.laboratory")

		if (any(!unique(pic_id) %in% learned_pics) & verbose==TRUE) print(paste0("SERIOUS WARNING: The picture ID '", setdiff(pic_id, learned_pics), "' has not been learned!"))

		if (length(pic_id) != length(sentences)) {
		  if (verbose==TRUE) print("WARNING: The length of pic_id is not equal to the number of sentences. Trying to recycle the pic_id vector.")
			pic_id <- rep_len(pic_id, length.out=length(sentences))
		}

		# dirty trick to get a full matrix, even when there is only one picture provided: Add a dummy picture, which later is deleted
		pic_id2 <- c("AAAAAAAAAAA", pic_id)

		dmy <- dummyVars(~pic_id2, data = data.frame(pic_id2))
		pic_dummy1 <- data.frame(predict(dmy, newdata = data.frame(pic_id2)))

		# remove extra picture
		pic_dummy2 <- pic_dummy1[-1, -1, drop=FALSE]
		colnames(pic_dummy2) <- gsub("pic_id2", "PIC_", colnames(pic_dummy2))

		# create missing variables (i.e., pictures that have been learned but are not in test sets): All other pictures get "0"
		missing_pics <- setdiff(learned_pics_features, colnames(pic_dummy2))
		missing_pics_matrix <- matrix(rep(0, length(missing_pics)*length(sentences)), nrow=length(sentences))
		colnames(missing_pics_matrix) <- missing_pics

		pic_dummy <- cbind(pic_dummy2, missing_pics_matrix)

	} else {
		pic_dummy <- NULL
	}

	## ======================================================================
	## Add LIWC features
	## ======================================================================

	# SENTENCE LEVEL
	# https://github.com/kbenoit/LIWCalike

	if ("LIWC" %in% type) {

		if (verbose==TRUE) print("Computing LIWC scores (sentence level) ...")
		liwcDict <- dictionary(file = system.file("extdata", "German_LIWC2001_Dictionary.dic", package = "AMC"), format = "LIWC", encoding = "Latin1")

		LIWCTEXT <- sentences
		LIWCTEXT[LIWCTEXT==""] <- "XYZ"	# empty strings break liwcalike
		output <- liwcalike(LIWCTEXT, liwcDict, what = "word", remove_symbols = TRUE, remove_punct = TRUE)

		colnames(output)[7:ncol(output)] <- paste0("LIWC_sentence_", colnames(output)[7:ncol(output)])

		LIWC_sentence <- cbind(UTID, USID, output[, 7:ncol(output)])

	}  # of LIWC in type


	## ======================================================================
	## Enrich the PSE_AMC data set with word embeddings from fasttext
	## ======================================================================

	# If you use these word vectors, please cite the following paper:
	# E. Grave*, P. Bojanowski*, P. Gupta, A. Joulin, T. Mikolov, Learning Word Vectors for 157 Languages

	if ("FASTTEXT" %in% type) {

		if (verbose==TRUE) print("Computing fasttext embeddings on sentence level ...")

		# write sentences to HDD; remove all special characters (some break fastText); do NOT lower case
	  tmp_file_in <- tempfile("FT", fileext=".txt")
	  tmp_file_out <- tempfile("FT", fileext=".txt")
	  con <- file(tmp_file_in)
		writeLines(str_replace_all(sentences, "[^a-zA-Z0-9\\u00f6\\u00e4\\u00fc\\u00df\\u00c4\\u00d6\\u00dc]", "?"), con)
		close(con)

		system(paste0(options("fasttext.binary.path"), " print-sentence-vectors ", options("fasttext.bin.path"), " < ", tmp_file_in, " > ", tmp_file_out))

		# load results
		FASTTEXT_sentence <- data.table::fread(tmp_file_out, header=FALSE, sep=" ")
		colnames(FASTTEXT_sentence) <- paste0("FASTTEXT_sentence_", 1:ncol(FASTTEXT_sentence))

		FASTTEXT_sentence <- cbind(UTID, USID, FASTTEXT_sentence)

		# remove temporary files
		unlink(tmp_file_in)
		unlink(tmp_file_out)
	}

	## ======================================================================
	## Special features
	## ======================================================================


	# questionmark_: Does the sentence contain a question mark?
	p_questionMark <- grepl("?", sentences, fixed=TRUE) %>% as.numeric()
	p_exclamationMark <- grepl("!", sentences, fixed=TRUE) %>% as.numeric()

	# combine extraFeatures
	extraFeatures <- bind_cols(
		UTID=UTID, USID=USID,
		data.frame(
			wc=tokenizers::count_words(sentences),
			sc=data.frame(USID) %>% group_by(USID) %>% mutate(n=n()) %>% pull(n),
			p_questionMark,
			p_exclamationMark
		),
		pic_dummy)

	if ("LIWC" %in% type) {
		#LIWC <- inner_join(LIWC_sentence %>% select(UTID, USID, contains("LIWC")), LIWC_story %>% select(USID, contains("LIWC")), by="USID") %>% ungroup()
		extraFeatures <- cbind(extraFeatures, LIWC_sentence %>% select(contains("LIWC")))
	}

	if ("FASTTEXT" %in% type) {
		#FASTTEXT <- inner_join(FASTTEXT_sentence, FASTTEXT_story, by="USID") %>% ungroup()
		extraFeatures <- cbind(extraFeatures, FASTTEXT_sentence %>% select(-UTID, -USID))
	}

	return(extraFeatures)
}
