% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/extra_features_functions.R
\name{computeExtraFeatures}
\alias{computeExtraFeatures}
\title{Title}
\usage{
computeExtraFeatures(
  sentences,
  type = c("LIWC", "FASTTEXT"),
  UTID = NULL,
  USID = NULL,
  pic_id = NULL,
  verbose = TRUE
)
}
\arguments{
\item{sentences}{A vector of sentences.}

\item{type}{Which types of extra features should be computed? Can be any of c("LIWC", "FASTTEXT"), or "" for none.}

\item{UTID}{Identifier for each sentence. This is passed through to the results object.}

\item{USID}{Identifier variable that groups sentences to stories (if USID is not provided, all sentences are a single story)}

\item{pic_id}{The picture id to which a story has been written.}

\item{verbose}{Print process information?}
}
\description{
Title
}
