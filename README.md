## How to install

### Development version

TODO: The project is not public yet.

```
remotes::install_gitlab("r-packages/amc", host="https://gitlab.lrz.de", subdir="package")
```


## fasttext

Install the fastText binary from https://fasttext.cc/docs/en/support.html

Download the German `cc.de.300.bin.gz` from to any location and `gunzip` it.


The location of the `fasttext` binary must be provided in the package options.


The pretrained word embeddings for German: https://fasttext.cc/docs/en/crawl-vectors.html

You can test your fasttext installation with that script in the console:

```
echo "Ich liebe dich sehr" | ./fasttext print-sentence-vectors /home/felix/.cache/R/AMC/cc.de.300.bin

# you should see 300 numbers representing the sentence vectors.

# query the trained model for words:
fasttext nn path_to/cc.de.300.bin
```

